<?php

/**
 * The API handler for handling API requests from themes and plugins using
 * the license manager.
 *
 * @package    Sky_License_Manager
 * @subpackage Sky_License_Manager/public
 * @author     KENT <thietke4rum@gmail.com>
 */
class Sky_License_Manager_API {

    /**
     * The handler function that receives the API calls and passes them on to the
     * proper handlers.
     *
     * @param $action   string  The name of the action
     * @param $params   array   Request parameters
     */
    public function handle_request( $action, $params ) {
        switch ( $action ) {
            case 'info':
                $response = $this->verify_license_and_execute( array( $this, 'product_info' ), $params );
                break;

            case 'get':
                $response = $this->verify_license_and_execute( array( $this, 'get_product' ), $params );
                break;

            default:
                $response = $this->error_response( 'No such API action' );
                break;
        }

        $this->send_response( $response );
    }

    /**
     * Returns a list of variables used by the API
     *
     * @return  array    An array of query variable names.
     */
    public function get_api_vars() {
        return array( 'l',  'e', 'p' );
    }

    //
    // API HANDLER FUNCTIONS
    //

    /**
     * Checks the parameters and verifies the license, then forwards the request to the
     * actual API request handlers.
     *
     * @param $action_function  callable    The function (or array with class and function) to call
     * @param $params           array       The WordPress request parameters.
     * @return array            API response.
     */
    private function verify_license_and_execute( $action_function, $params ) {
        if ( ! isset( $params['p'] ) || ! isset( $params['e'] ) || ! isset( $params['l'] ) ) {
            return $this->error_response( 'Invalid request' );
        }

        $product_id = $params['p'];
        $email = $params['e'];
        $license_key = $params['l'];

        // Find product
        $posts = get_posts(
            array (
                'name' => $product_id,
                'post_type' => 'wplm_product',
                'post_status' => 'publish',
                'numberposts' => 1
            )
        );

        if ( ! isset( $posts[0] ) ) {
            return $this->error_response( 'Product not found.' );
        }

        // Verify license
        if ( ! $this->verify_license( $posts[0]->ID, $email, $license_key ) ) {
            return $this->error_response( 'Invalid license or license expired.' );
        }

        // Call the handler function
        return call_user_func_array( $action_function, array( $posts[0], $product_id, $email, $license_key ) );
    }

    /**
     * The handler for the "info" request. Checks the user's license information and
     * returns information about the product (latest version, name, update url).
     *
     * @param   $product        WP_Post   The product object
     * @param   $product_id     string    The product id (slug)
     * @param   $email          string    The email address associated with the license
     * @param   $license_key    string  The license key associated with the license
     *
     * @return  array           The API response as an array.
     */
    private function product_info( $product, $product_id, $email, $license_key ) {
        // Collect all the metadata we have and return it to the caller
        $meta         = get_post_meta( $product->ID, 'wp_license_manager_product_meta', true );
        
        $version      = isset( $meta['version'] ) ? $meta['version'] : '';
        $tested       = isset( $meta['tested'] ) ? $meta['tested'] : '';
        $last_updated = isset( $meta['updated'] ) ? $meta['updated'] : '';
        $author       = isset( $meta['author'] ) ? $meta['author'] : '';
        $banner_low   = isset( $meta['banner_low'] ) ? $meta['banner_low'] : '';
        $banner_high  = isset( $meta['banner_high'] ) ? $meta['banner_high'] : '';
        $item_id      = isset( $meta['item_id'] ) ? $meta['item_id'] : '';
        // $file_bucket  = isset( $meta['file_bucket'] ) ? $meta['file_bucket'] : '';

        return array(
            'name'            => $product->post_title,
            'description'     => $product->post_content,
            'version'         => $version,
            'tested'          => $tested,
            'author'          => $author,
            'last_updated'    => $last_updated,
            'banner_low'      => $banner_low,
            'banner_high'     => $banner_high,
            'item_id'         => $item_id,
            "package_url"   => home_url( '/api/license-manager/v1/get?p=' . $product_id . '&e=' . $email . '&l=' . urlencode( $license_key ) ),
            // "package_url"     => "https://s3.amazonaws.com/{$file_bucket}/{$product_id}.zip",
            "description_url" => get_permalink( $product->ID ) . '#v=' . $version
        );
    }

    /**
     * The handler for the "get" request. Redirects to the file download.
     *
     * @param   $product    WP_Post     The product object
     */
    private function get_product( $product, $product_id, $email, $license_key ) {
        // Get the AWS data from post meta fields
        $meta      = get_post_meta( $product->ID, 'wp_license_manager_product_meta', true );
        $bucket    = isset ( $meta['file_bucket'] ) ? urlencode($meta['file_bucket']) : '';
        $file_name = isset ( $meta['file_name'] ) ? urlencode($meta['file_name']) : '';

        if ( $bucket == '' || $file_name == '' ) {
            // No file set, return error
            return $this->error_response( 'No download defined for product.' );
        }

        // Use the AWS API to set up the download
        // This API method is called directly by WordPress so we need to adhere to its
        // requirements and skip the JSON. WordPress expects to receive a ZIP file...

        $s3_url = Sky_License_Manager_S3::get_s3_url( $bucket, $file_name );
        wp_redirect( $s3_url, 302 );
        exit;
    }

    //
    // HELPER FUNCTIONS
    //

    /**
     * Looks up a license that matches the given parameters.
     *
     * @param $product_id   int     The numeric ID of the product.
     * @param $email        string  The email address attached to the license.
     * @param $license_key  string  The license key
     * @return mixed                The license data if found. Otherwise false.
     */
    private function find_license( $product_id, $email, $license_key ) {
        global $wpdb;

        $table_name = $wpdb->prefix . 'product_licenses';

        $purchase_info = $this->envato_verify_purchase($license_key);
        $item_id_product = get_post_meta( $product_id, 'wp_license_manager_product_meta', true );

        // Check license with the parent product
        if( !empty($item_id_product['parent']) && is_numeric( $item_id_product['parent'] ) ) {
            $parent = get_post( $item_id_product['parent'] );
            if( $parent && $parent->post_type == 'wplm_product' && $parent->post_status == 'publish' ) {
                $item_id_product = get_post_meta( $parent->ID, 'wp_license_manager_product_meta', true );
                $product_id = $parent->ID;
            }
        }
        
        if( $purchase_info != 'NONE' && is_array($purchase_info) ) {

            // Envato customer -> confirm that license is added to database
            if ( $purchase_info['item_id'] == $item_id_product['item_id'] ) {

                $check_key = $wpdb->get_results(
                $wpdb->prepare( "SELECT * FROM $table_name WHERE product_id = %d AND email = '%s' AND license_key = '%s'",
                    $product_id, $email, $license_key ), ARRAY_A);
                if ( count( $check_key ) < 1 ) {
                    $wpdb->query( $wpdb->prepare( 
                        "
                            INSERT INTO $table_name
                            ( product_id, email, license_key )
                            VALUES ( %d, %s, %s )
                        ", 
                            array(
                            $product_id, 
                            $email, 
                            $license_key
                        ) 
                    ) );
                }
            }
        }

        $licenses = $wpdb->get_results(
            $wpdb->prepare( "SELECT * FROM $table_name WHERE product_id = %d AND email = '%s' AND license_key = '%s'",
                $product_id, $email, $license_key ), ARRAY_A);

        if ( count( $licenses ) > 0 ) {
            return $licenses[0];
        }        

        return false;
    }
    /* -------------------------------------------------------
     * Verify key in themeforest
     * ------------------------------------------------------- */
    private function envato_verify_purchase($purchase_code = null){
        //SETUP THE API DATA

        $username = 'NooTheme'; 
        $api_key = '4f88zxilrgdqu1wphtyc2904ms5e12jh';

        //CHECK IF THE CALL FOR THE FUNCTION WAS EMPTY
        if ( $purchase_code != '' ):

            /*
                STEPS IN THE CODE BELOW:
                 - QUERY ENVATO API FOR JSON RESULT
                 - DECODE THE RESULT AND TRANSFORM IT FROM OBJECTS TO AN ARRAY
                 - CHECK IF THERE IS A ITEM TITLE == THE PURCHASE WAS MADE OR NOT
            */            

            //$result_from_json = file_get_contents('http://marketplace.envato.com/api/edge/'.$username.'/'.$api_key.'/verify-purchase:'.$purchase_code.'.json');
            //$result = json_decode($result_from_json, true);
            
            $URL = 'http://marketplace.envato.com/api/edge/'.$username.'/'.$api_key.'/verify-purchase:'.$purchase_code.'.json';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            curl_setopt($ch, CURLOPT_URL, $URL);
            $data = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($data, true);
            
            if ( @$result['verify-purchase']['item_name'] )
                return $result['verify-purchase'];
        endif;
        return 'NONE';
    }

    /**
     * Checks whether a license with the given parameters exists and is still valid.
     *
     * @param $product_id   int     The numeric ID of the product.
     * @param $email        string  The email address attached to the license.
     * @param $license_key  string  The license key.
     * @return bool                 true if license is valid. Otherwise false.
     */
    private function verify_license( $product_id, $email, $license_key ) {
        $license = $this->find_license( $product_id, $email, $license_key );
        if ( ! $license ) {
            return false;
        }

        $valid_until = strtotime( $license['valid_until'] );
        if ( $license['valid_until'] != '0000-00-00 00:00:00' && time() > $valid_until ) {
            return false;
        }

        return true;
    }

    /**
     * Generates and returns a simple error response. Used to make sure every error
     * message uses same formatting.
     *
     * @param $msg      string  The message to be included in the error response.
     * @return array    The error response as an array that can be passed to send_response.
     */
    private function error_response( $msg ) {
        return array( 'error' => $msg );
    }

    /**
     * Prints out the JSON response for an API call.
     *
     * @param $response array   The response as associative array.
     */
    private function send_response( $response ) {
        echo json_encode( $response );
    }

}