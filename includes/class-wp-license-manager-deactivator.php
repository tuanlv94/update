<?php

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @package    Sky_License_Manager
 * @subpackage Sky_License_Manager/includes
 * @author     KENT <thietke4rum@gmail.com>
 */
class Sky_License_Manager_Deactivator {

	/**
	 * Does deactivation actions.
	 */
	public static function deactivate() {

	}

}
