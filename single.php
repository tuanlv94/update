<?php 
if ( $_SERVER['HTTP_REFERER'] ) :
	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();
			the_content();
		endwhile;
	endif;
else :
	wp_redirect('http://skygame.mobi', 301);
endif;
?>