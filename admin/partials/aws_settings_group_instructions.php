<?php
/**
 * The view for the AWS settings section's description on the plugin's settings page.
 *
 * @package    Sky_License_Manager
 * @subpackage Sky_License_Manager/admin/partials
 */
?>

<?php _e( 'Enter your AWS credentials below.', $this->plugin_name ); ?>