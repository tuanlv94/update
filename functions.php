<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Needed for listing licenses
if ( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * The code that runs during plugin activation.
 */
require_once 'includes/class-wp-license-manager-activator.php';

/**
 * The code that runs during plugin deactivation.
 */
require_once 'includes/class-wp-license-manager-deactivator.php';

/** This action is documented in includes/class-wp-license-manager-activator.php */
register_activation_hook( __FILE__, array( 'Sky_License_Manager_Activator', 'activate' ) );

/** This action is documented in includes/class-wp-license-manager-deactivator.php */
register_deactivation_hook( __FILE__, array( 'Sky_License_Manager_Deactivator', 'deactivate' ) );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require_once 'includes/class-wp-license-manager.php';


if ( ! function_exists( 'write_log' ) ) {
    function write_log( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }
}

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 */
function run_wp_license_manager() {

	$plugin = new Sky_License_Manager();
	$plugin->run();

}
run_wp_license_manager();

/** ====== ADD COLUMN NEW IN CUSTOM POST TYPE ====== **/

    /* -------------------------------------------------------
     * Create functions custom_colums_product
     * ------------------------------------------------------- */
    if ( ! function_exists( 'custom_columns_product_head' ) ) :   
        
        function custom_columns_product_head($defaults) {
            $defaults['version'] = 'Version';
            return $defaults;
        }

    endif;

    /** ====== END custom_colums_product ====== **/

    /* -------------------------------------------------------
     * Create functions custom_columns
     * ------------------------------------------------------- */

    if ( ! function_exists( 'custom_columns' ) ) :
        
        function custom_columns_product_content( $column_name, $post_id ) {
            $list_meta = get_post_meta($post_id, 'wp_license_manager_product_meta');
            // var_dump($list_meta);
            switch ( $column_name ) {
                case 'version':
                    echo $list_meta[0]['version'];
                    break;
            }

        }

    endif;

    /** ====== END custom_columns ====== **/

    /* -------------------------------------------------------
     * Create functions custom_edit_column_product
     * ------------------------------------------------------- */

    if ( ! function_exists( 'custom_edit_column_product' ) ) :
        
        function custom_edit_column_product( $columns ) {
            
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'title' => __( 'List Product' ),
                'version' => __( 'Version' ),
                'author' => __( 'Author' ),
                'date' => __( 'Date' )
            );

            return $columns;

        }

    endif;

    /** ====== END custom_edit_column_product ====== **/

    add_filter('manage_wplm_product_posts_columns', 'custom_columns_product_head'); // -- Get header column
    add_action( 'manage_wplm_product_posts_custom_column' , 'custom_columns_product_content', 10, 2 ); // -- Get content in column
    add_filter( 'manage_edit-wplm_product_columns', 'custom_edit_column_product' ); // -- Change location display column

/** ====== END ADD COLUMN NEW IN CUSTOM POST TYPE ====== **/


/** ====== QUICK EDIT ====== **/

    /* -------------------------------------------------------
     * Create functions display_quick_edit_custom_product
     * Show input on click Quick Edit
     * ------------------------------------------------------- */

    if ( ! function_exists( 'display_quick_edit_custom_product' ) ) :

        function display_quick_edit_custom_product($column_name, $post_type){
            ?>
            <fieldset class="inline-edit-col-right inline-edit-book">
              <div class="inline-edit-col column-<?php echo $column_name; ?>">
                <label class="inline-edit-group">
                <?php 
                switch ( $column_name ) {
                    case 'version': ?>
                        <span class="title">Version</span>
                        <input name="version" value=""/>
                    <?php break;
                 }
                ?>
                </label>
              </div>
            </fieldset>
            <?php
        }

        add_action('quick_edit_custom_box', 'display_quick_edit_custom_product', 10, 2);
   
    endif;

    /** ====== END display_quick_edit_custom_product ====== **/

    /* -------------------------------------------------------
     * Create functions save_quick_edit_custom_product
     * ------------------------------------------------------- */
    
    if ( ! function_exists( 'save_quick_edit_custom_product' ) ) :
        
        function save_quick_edit_custom_product( $post_id ) {
            
            $slug = 'wplm_product';

            if ( !isset( $_POST['post_type'] ) || $slug !== $_POST['post_type'] ) return; // -- Check slug

            if ( !current_user_can( 'edit_post', $post_id ) ) return; // -- Check user can editing post

            // if ( !wp_verify_nonce( $_POST["{$slug}_edit_nonce"], plugin_basename( __FILE__ ) ) ) return;

            if ( isset( $_REQUEST['version'] ) ) :

                $info_version = get_post_meta( $post_id, 'wp_license_manager_product_meta', true );
                $info_version['version'] = $_REQUEST['version'];
                update_post_meta( $post_id, 'wp_license_manager_product_meta', $info_version );
            endif;

            // var_dump($info_version); die;
    
        }

        add_action( 'save_post', 'save_quick_edit_custom_product' );
    
    endif;
    
    /** ====== END save_quick_edit_custom_product ====== **/

    /* -------------------------------------------------------
     * Create functions load_enqueue_scripts_edit_quick_custom_product
     * Load Scripts
     * ------------------------------------------------------- */
    
    if ( ! function_exists( 'load_enqueue_scripts_edit_quick_custom_product' ) ) :
        
        function load_enqueue_scripts_edit_quick_custom_product( $hook ) {
            
            if ( 'edit.php' === $hook && isset( $_GET['post_type'] ) && $_GET['post_type'] === 'wplm_product' ) :

                // wp_enqueue_script( 'custom_script', plugins_url('scripts/admin_edit.js', __FILE__), false, null, true );

            endif;
    
        }
        add_action( 'admin_enqueue_scripts', 'load_enqueue_scripts_edit_quick_custom_product' );
    
    endif;
    
    /** ====== END load_enqueue_scripts_edit_quick_custom_product ====== **/
    
/** ====== END QUICK EDIT ====== **/